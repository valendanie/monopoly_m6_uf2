package hibernateproject;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "jugador")
public class Jugador {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_jugador")
	private int id;
	@Column(name = "victorias")
	private int victorias;
	@Column(name = "nom_jugador")
	private String nom;
	@Column(name = "eliminat")
	private boolean eliminat;
	@Column(name = "ordre_tirada")
	private int ordre_tirada;
	@Column(name = "diners")
	private int diners;
	@Column(name = "quantitat propietats")
	private int quantitat_propietats;
}

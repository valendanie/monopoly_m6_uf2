package hibernateproject;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "Propietat")

public class Propietat {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_propietat")
	private int id;
	@Column(name = "nom")
	private String NOM;
	@Column(name = "preu")
	private int preu;
	@Column(name = "preu_casa")
	private int preu_casa;
	@Column(name = "preu_hipoteca")
	private int preu_hipoteca;
	@Column(name = "hipotecat")
	private boolean hipotecat;
	@Column(name = "posicio")
	private int posicio;
	@Column(name = "lloguer")
	private int lloguer;

}
